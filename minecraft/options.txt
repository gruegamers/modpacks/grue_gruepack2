version:1343
invertYMouse:false
mouseSensitivity:0.5
fov:0.0
gamma:0.0
saturation:0.0
renderDistance:8
guiScale:2
particles:0
bobView:true
anaglyph3d:false
maxFps:260
fboEnable:true
difficulty:1
fancyGraphics:true
ao:2
renderClouds:true
resourcePacks:[]
incompatibleResourcePacks:[]
lastServer:
lang:en_us
chatVisibility:0
chatColors:true
chatLinks:true
chatLinksPrompt:true
chatOpacity:1.0
snooperEnabled:true
fullscreen:false
enableVsync:true
useVbo:true
hideServerAddress:false
advancedItemTooltips:false
pauseOnLostFocus:true
touchscreen:false
overrideWidth:0
overrideHeight:0
heldItemTooltips:true
chatHeightFocused:1.0
chatHeightUnfocused:0.44366196
chatScale:1.0
chatWidth:1.0
mipmapLevels:0
forceUnicodeFont:false
reducedDebugInfo:false
useNativeTransport:true
entityShadows:true
mainHand:right
attackIndicator:2
showSubtitles:false
realmsNotifications:false
enableWeakAttacks:false
autoJump:false
narrator:0
tutorialStep:movement
key_key.attack:-100
key_key.use:-99
key_key.forward:17
key_key.left:30
key_key.back:31
key_key.right:32
key_key.jump:57
key_key.sneak:42
key_key.sprint:29
key_key.drop:16
key_key.inventory:18
key_key.chat:20
key_key.playerlist:15
key_key.pickItem:-98
key_key.command:53
key_key.screenshot:60
key_key.togglePerspective:63
key_key.smoothCamera:0
key_key.fullscreen:87
key_key.spectatorOutlines:0
key_key.swapHands:33
key_key.saveToolbarActivator:0
key_key.loadToolbarActivator:0
key_key.advancements:38:SHIFT
key_key.hotbar.1:2
key_key.hotbar.2:3
key_key.hotbar.3:4
key_key.hotbar.4:5
key_key.hotbar.5:6
key_key.hotbar.6:7
key_key.hotbar.7:8
key_key.hotbar.8:9
key_key.hotbar.9:10
key_of.key.zoom:44
key_Change Caster Focus:33:SHIFT
key_Misc Caster Toggle:34:SHIFT
key_keybind.baublesinventory:25
key_bloodmagic.keybind.open_holding:35:SHIFT
key_bloodmagic.keybind.cycle_holding_pos:13:SHIFT
key_bloodmagic.keybind.cycle_holding_neg:12:SHIFT
key_roots.keybinds.pouch:0
key_roots.keybinds.quiver:0
key_roots.keybinds.spell_slot_1:0
key_roots.keybinds.spell_slot_3:0
key_roots.keybinds.spell_slot_next:0
key_roots.keybinds.spell_slot_2:0
key_roots.keybinds.spell_slot_previous:0
key_roots.keybinds.spell_slot_4:0
key_roots.keybinds.spell_slot_5:0
key_key.conarm.toggle_helm.desc:71
key_key.conarm.toggle_chest.desc:75
key_key.conarm.toggle_leg.desc:79
key_cos.key.opencosarmorinventory:0
key_psimisc.keybind:46
key_key.moreoverlays.lightoverlay.desc:65
key_key.moreoverlays.chunkbounds.desc:67
key_key.thebomplugin.scrollmodifier:56
key_key.wearablebackpacks.open:24
key_smoothfont.key.guiOpen:199
key_key.jei.toggleOverlay:24:CONTROL
key_key.jei.focusSearch:33:CONTROL
key_key.jei.toggleCheatMode:0
key_key.jei.toggleEditMode:0
key_key.jei.showRecipe:19
key_key.jei.showUses:22
key_key.jei.recipeBack:14
key_key.jei.previousPage:201
key_key.jei.nextPage:209
key_key.jei.bookmark:30
key_key.jei.toggleBookmarkOverlay:0
key_keys.armourers_workshop.undo:0:CONTROL
key_keys.armourers_workshop.open-wardrobe:25
key_nei.options.keys.gui.botania_corporea_request:46
key_key.clienttweaks.disable_step_assist:0
key_key.clienttweaks.hide_offhand_item:0
key_waila.keybind.wailaconfig:82
key_waila.keybind.wailadisplay:0
key_waila.keybind.liquid:0
key_waila.keybind.recipe:19:SHIFT
key_waila.keybind.usage:22:ALT
key_key.clipboardPaste:210
key_key.gravelminer.toggle:0
key_invtweaks.key.sort:19
key_LevelUpGUI:38:CONTROL
key_key.rotateup:200
key_key.rotatedown:208
key_key.rotateright:205
key_key.rotateleft:203
key_key.little.flip:34
key_key.little.mark:50
key_key.little.config.item:46
key_key.little.undo:0:CONTROL
key_key.little.redo:21:CONTROL
key_key.refinedstorage.focusSearchBar:15
key_key.refinedstorage.clearGridCraftingMatrix:45:CONTROL
key_key.refinedstorage.openWirelessGrid:0
key_key.refinedstorage.openWirelessFluidGrid:0
key_key.refinedstorage.openPortableGrid:0
key_key.refinedstorage.openWirelessCraftingMonitor:0
soundCategory_master:1.0
soundCategory_music:0.0
soundCategory_record:1.0
soundCategory_weather:1.0
soundCategory_block:1.0
soundCategory_hostile:1.0
soundCategory_neutral:1.0
soundCategory_player:1.0
soundCategory_ambient:1.0
soundCategory_voice:1.0
soundCategory_ds_footsteps:1.0
soundCategory_ds_biome:1.0
modelPart_cape:true
modelPart_jacket:true
modelPart_left_sleeve:true
modelPart_right_sleeve:true
modelPart_left_pants_leg:true
modelPart_right_pants_leg:true
modelPart_hat:true
